import {ChangeDetectorRef, Component} from '@angular/core';
import {Generic} from "../core/generic";
import {RegisterService} from "../core/register.service";
import {NgSelectConfig} from "@ng-select/ng-select";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  hasToken: Generic = new Generic(false);
  title = 'gestor';


  menuBookmaker = [
    {label: 'Local', link: 'bookmaker', classIcon: 'fas fa-home'},
    {label: 'Associar OddsMaker', link: 'associateOddsmaker', classIcon: 'fas fa-exchange-alt'}
  ]

  menu = [
    {label: 'Provedor', link: '#', classIcon: 'fas fa-project-diagram', dropdown: this.menuBookmaker, dropdownActive: false},
    {label: 'País', link: 'country', classIcon: 'fas fa-globe-americas'},
    {label: 'Esportes', link: 'sport', classIcon: 'fas fa-futbol'},
    {label: 'Time', link: 'team', classIcon: 'fas fa-flag'},
    {label: 'Torneio', link: 'tournament', classIcon: 'fas fa-tag'},
    {label: 'Filtros', link: 'filter', classIcon: 'fas fa-filter'}
  ];


  constructor (
    private registerService: RegisterService,
    private cdRef: ChangeDetectorRef,
    private configSelect: NgSelectConfig) {
    this.configSelect.notFoundText = 'Nenhum dado encontrado';
    this.configSelect.addTagText = 'Adicionar item ';
  }

  ngOnInit() {
    if (localStorage.rpToken !== null && localStorage.rpToken !== undefined) {
      this.registerService.changeHasToken(true);
    }
  }

  setWatchHasToken() {
    this.registerService.watchHasToken().subscribe(res => this.hasToken = res);
    this.cdRef.detectChanges();
  }

  ngAfterViewChecked(): void {
    this.setWatchHasToken();
  }




}
