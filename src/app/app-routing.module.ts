import { NgModule } from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import {AppComponent} from "./app.component";

const appRoutes: Routes = [

  // {path: 'home/:token', component: HomeComponent}
  // {
  //   path: '',
  //   component: AppComponent
    // ,
    // runGuardsAndResolvers: 'always',
    // children: [
    //   {path: 'partner', loadChildren: () => import('./pages/partner/partner.module').then(m => m.PartnerModule)}
    // ]
  // }
]

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
  // providers: [
  //   {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
  //   { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
  //
  // ]

  // declarations: [],
  // imports: [
  //   CommonModule
  // ]


})
export class AppRoutingModule { }
