import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CommonService} from '../../../core/common.service';
import {Country} from '../country/country';
import {Partner} from './partner';


@Injectable({
  providedIn: 'root'
})
export class PartnerService extends CommonService<Partner> {

  constructor(
    protected http: HttpClient
  ) {
    super(http);
    this.serviceName = 'gestor';
    this.resourceName = 'partner';
  }

  createPartner(country: Country) {
    return this.create(country);
  }

  getAll() {
    const filter = encodeURIComponent('{"id": {"$gt": 0}}');
    return this.find('/list?filter=' + filter);
  }

  getPartner(id: number) {
    return this.find('/' + id);
  }

  updatePartner(country: Country) {
    return this.update(country, '/' + country.id);
  }

  deletePartner(id) {
    return this.delete(id);
  }


}
