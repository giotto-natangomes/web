import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ComponentsModule} from '../../../components/components.module';
import {PartnerComponent} from './partner.component';
import {PartnerFormComponent} from './partner-form/partner-form.component';
import {ListPartnerComponent} from './list-partner/list-partner.component';
import {PartnerRoutingModule} from './partner-routing.module';

@NgModule({
  declarations: [
    PartnerComponent,
    PartnerFormComponent,
    ListPartnerComponent
  ],
  imports: [
    CommonModule,
    PartnerRoutingModule,
    ComponentsModule,
    FormsModule,
    ReactiveFormsModule
  ],
})
export class PartnerModule {
}
