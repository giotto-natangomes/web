import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PartnerComponent } from './partner.component';
import {ListPartnerComponent} from './list-partner/list-partner.component';
import {PartnerFormComponent} from './partner-form/partner-form.component';
import {AuthGuardService} from '../../../core/auth-guard.service';

const partnerRoutes: Routes = [
  { path: 'partner', component: PartnerComponent, canActivate: [AuthGuardService],
    children: [
      { path: '',     component: ListPartnerComponent },
      { path: 'form', component: PartnerFormComponent },
      { path: 'edit/:id', component: PartnerFormComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(partnerRoutes)],
  exports: [RouterModule]
})
export class PartnerRoutingModule { }
