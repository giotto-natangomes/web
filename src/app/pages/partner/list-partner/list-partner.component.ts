import {Component, OnInit} from '@angular/core';
import {PartnerService} from '../partner.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-list-partner',
  templateUrl: './list-partner.component.html',
  styleUrls: ['./list-partner.component.scss']
})

export class ListPartnerComponent implements OnInit {
  columns = [
    {data: 'id', label: 'ID'},
    {data: 'name', label: 'Nome'}
  ];
  data = [];
  loading = true;
  actions = ['create', 'delete', 'edit'];

  constructor(private partnerService: PartnerService) {
  }

  ngOnInit() {
    let THIS = this;

    this.partnerService.getAll().subscribe({
      next(res) {
        if (res['status'] === 200) {
          res['data'].items.forEach(country => {
            THIS.data.push(country);
          });
          THIS.loading = false;
        }
      },
      error(error) {
        THIS.partnerService.messagesError(error, THIS.partnerService.resourceName);
      }
    });
  }

  delete(partnerId) {
    Swal.fire({
      title: 'Tem certeza?',
      text: 'Você realmente deseja excluir este parceiro permanentemente?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Sim, excluir agora!',
      cancelButtonText: 'Não!',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        this.deleteAction(partnerId);
      } else {
        Swal.fire({
          title: 'Cancelado',
          text: 'Operação cancelada com sucesso',
          type: 'error'
        });
      }
    });
  }

  deleteAction(partnerId) {
    const THIS = this;
    this.loading = true;

    this.partnerService.deletePartner('/' + partnerId).subscribe({
      next(resp) {
        THIS.removePartner(partnerId);
        THIS.loading = false;
        Swal.fire({
          title: 'Feito!',
          text: 'Parceiro excluído com sucesso!',
          type: 'success',
          confirmButtonText: 'Ok'
        });
      },
      error(error) {
        THIS.loading = false;
        THIS.partnerService.messagesError(error, THIS.partnerService.resourceName);
      }
    });
  }

  removePartner(partnerId) {
    this.data.forEach((partner, index) => {
      if (partner.id === partnerId) {
        this.data.splice(index, 1);
      }
    });
  }
}
