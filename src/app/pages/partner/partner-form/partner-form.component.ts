import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Partner} from '../partner';
import {PartnerService} from '../partner.service';
import {ActivatedRoute} from '@angular/router';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-partner-form',
  templateUrl: './partner-form.component.html',
  styleUrls: ['./partner-form.component.scss']
})

export class PartnerFormComponent implements OnInit {
  partner: Partner = new Partner();
  loading = false;
  formulario: FormGroup;
  edit = false;
  submitForm = false;

  constructor(private partnerService: PartnerService,
              private fb: FormBuilder,
              private route: ActivatedRoute,
              private toast: ToastrService) { }

  ngOnInit(): void {
    this.getId();
  }

  getId() {
    this.route.paramMap.subscribe(params => {
      const id = params.get('id');
      if (id != null) {
        this.edit = true;
        this.loading = true;
        this.createForm();
        this.getPartner(id);
      } else {
        this.createForm();
      }
    });
  }

  onSubmit() {
    this.loading = true;
    this.submitForm = true;

    if (!this.formulario.valid) {
      this.loading = false;
      this.toast.error('Preencha todos os campos marcados como obrigatório (*)', 'Erro!', {
        closeButton: true
      });
    } else if (this.edit) {
      this.editPartner();
    } else {
      this.createPartner();
    }
  }

  getPartner(id) {
    const THIS = this;

    this.partnerService.getPartner(id).subscribe({
      next(success) {
        THIS.loading = false;
        THIS.createFormEdit(success['data']);
      },
      error(error) {
        THIS.loading = false;
        THIS.partnerService.messagesError(error, THIS.partnerService.resourceName);
      }
    });
  }

  createFormEdit(partner) {
    this.formulario = this.fb.group({
      id: [partner.id, Validators.compose([Validators.required])],
      name: [partner.name, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(80)])]
    });
  }

  createForm() {
    this.formulario = this.fb.group({
      name: [null, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(80)])]
    });
  }

  createPartner() {
    const THIS = this;

    this.partnerService.createPartner(this.formulario.value).subscribe({
      next(success) {
        THIS.loading = false;
        THIS.toast.success('Parceiro cadastrado com sucesso', 'Sucesso!', {
          closeButton: true
        });
        THIS.submitForm = false;
        THIS.formulario.reset();
      },
      error(error) {
        THIS.loading = false;
        THIS.partnerService.messagesError(error, THIS.partnerService.resourceName);
      }
    });
  }

  editPartner() {
    const THIS = this;

    this.partnerService.updatePartner(this.formulario.value).subscribe({
      next(success) {
        THIS.loading = false;
        THIS.toast.success('Parceiro atualizado com sucesso', 'Sucesso!', {
          closeButton: true
        });
      },
      error(error) {
        THIS.loading = false;
        THIS.partnerService.messagesError(error, THIS.partnerService.resourceName);
      }
    });
  }

  clearForm() {
    this.submitForm = false;
    this.formulario.reset();
  }

  get er_name() { return this.formulario.get('name') }
}
