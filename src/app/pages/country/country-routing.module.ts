import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CountryComponent } from './country.component';
import { ListCountryComponent } from './list-country/list-country.component';
import { CountryFormComponent } from './country-form/country-form.component';
// import {BookmakerComponent} from '../bookmaker/bookmaker.component';
import {AuthGuardService} from '../../../../src/core/auth-guard.service';


const countryRoutes: Routes = [
  { path: 'country', component: CountryComponent, canActivate: [AuthGuardService],
    children: [
      { path: '',           component: ListCountryComponent },
      { path: 'form',       component: CountryFormComponent },
      { path: 'edit/:id',   component: CountryFormComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(countryRoutes)],
  exports: [RouterModule]
})
export class CountryRoutingModule { }
