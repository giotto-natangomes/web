
export class Country {
    constructor(name: string = '') {
        this.name = name;
    }
    
    id: number;
    name: string;
}
