import { Injectable } from '@angular/core';
import { Country } from './country';
import { HttpClient } from '@angular/common/http';
import { CommonService } from '../../../core/common.service';


@Injectable({
  providedIn: 'root'
})
export class CountryService extends CommonService<Country> {

  constructor(protected http: HttpClient) {
    super(http);
    this.serviceName = 'gestor';
    this.resourceName = 'country';
  }

  createCountry(country: Country) {
    return this.create(country);
  }

  getAll() {
    const filter = encodeURIComponent('{"id": {"$gt": 0}}');
    return this.find('/list?filter=' + filter);
  }

  getCountry(id: number) {
    return this.find('/' + id);
  }

  updateCountry(country: Country) {
    return this.update(country, '/' + country.id);
  }

  deleteCountry(id) {
    return this.delete(id);
  }

}
