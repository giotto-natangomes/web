import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CountryRoutingModule } from './country-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { CountryComponent } from './country.component';
import { CountryFormComponent } from './country-form/country-form.component';
import { ListCountryComponent } from './list-country/list-country.component';
import { ComponentsModule } from '../../../components/components.module';
import {OwlDateTimeModule, OwlNativeDateTimeModule} from 'ng-pick-datetime';
import { GridFilterComponent } from 'src/components/grid-filter/grid-filter.component';

@NgModule({
  declarations: [
    CountryComponent,
    CountryFormComponent,
    ListCountryComponent,
    GridFilterComponent
  ],
  imports: [
    CommonModule,
    CountryRoutingModule,
    ComponentsModule,
    FormsModule,
    ReactiveFormsModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule
  ],
})
export class CountryModule { }
