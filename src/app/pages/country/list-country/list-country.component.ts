import { Component, OnInit } from '@angular/core';
import { CountryService } from '../country.service';
import Swal from 'sweetalert2';
import {Fields} from '../../../../../src/components/grid-filter/fields';

@Component({
  selector: 'rp-list-country',
  templateUrl: './list-country.component.html',
  styleUrls: ['./list-country.component.scss']
})

export class ListCountryComponent implements OnInit {
  columns = [
    {data: 'id', label: 'ID'},
    {data: 'name', label: 'Nome'}
  ];

  // filters: Fields[] = Array();
  data = [];
  loading = true;
  actions = ['view', 'create', 'delete', 'edit'];

  constructor(private countryService: CountryService) {}

  ngOnInit() {
    // this.filters.push(new Fields('text', [], [], false, 'teste', ''));
    let THIS = this;

    this.countryService.getAll().subscribe({
      next(res) {
        if (res['status'] === 200) {
          res['data'].items.forEach(country => {
            THIS.data.push(country);
          });
          THIS.loading = false;
        }
      },
      error(error) {
        THIS.countryService.messagesError(error, THIS.countryService.resourceName);
      }
    });
  }

  delete(countryId) {
    Swal.fire({
      title: 'Tem certeza?',
      text: 'Você realmente deseja excluir este país permanentemente?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Sim, excluir agora!',
      cancelButtonText: 'Não!',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        this.deleteAction(countryId);
      }else{
        Swal.fire({
          title: 'Cancelado',
          text: 'Operação cancelada com sucesso',
          type: 'error'
        })
      }
    })
  }

  deleteAction(countryId) {
    const THIS = this;
    this.loading = true;

    this.countryService.deleteCountry('/' + countryId).subscribe({
      next(resp) {
        THIS.removeCountry(countryId);
        THIS.loading = false;
        Swal.fire({
          title: 'Feito!',
          text: 'País excluído com sucesso!',
          type: 'success',
          confirmButtonText: 'Ok'
        });
      },
      error(error){
        THIS.loading = false;
        THIS.countryService.messagesError(error, THIS.countryService.resourceName);
      }
    });
  }

  removeCountry(countryId){
    this.data.forEach( (country, index) => {
      if(country.id === countryId){
        this.data.splice(index, 1);
      }
    })
  }

}
