import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Country } from '../country';
import { CountryService } from '../country.service';
// import Swal from 'sweetalert2';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {ToastrService} from 'ngx-toastr';


@Component({
  selector: 'app-country-form',
  templateUrl: './country-form.component.html',
  styleUrls: ['./country-form.component.scss']
})

export class CountryFormComponent implements OnInit {
  country: Country = new Country();
  loading = false;
  formulario: FormGroup;
  edit = false;
  submitForm = false;

  constructor(private countryService: CountryService,
              private fb: FormBuilder,
              private route: ActivatedRoute,
              private toast: ToastrService) { }

  ngOnInit(): void {
    this.getId();
    this.createForm();
  }

  getId() {
    this.route.paramMap.subscribe(params => {
      const id = params.get('id');
      if (id != null) {
        this.edit = true;
        this.loading = true;
        this.createForm();
        this.getCountry(id);
      } else {
        this.createForm();
      }
    });
  }

  onSubmit() {
    this.loading = true;
    this.submitForm = true;

    if (!this.formulario.valid) {
      this.loading = false;
      this.toast.error('Preencha todos os campos marcados como obrigatório (*)', 'Erro!', {
        closeButton: true
      });
    } else if (this.edit) {
      this.editCountry();
    } else {
      this.createCountry();
    }
  }

  getCountry(id) {
    const THIS = this;

    this.countryService.getCountry(id).subscribe({
      next(success) {
        THIS.loading = false;
        THIS.createFormEdit(success['data']);
      },
      error(error) {
        THIS.loading = false;
        THIS.countryService.messagesError(error, THIS.countryService.resourceName);
      }
    });
  }

  createFormEdit(country) {
    this.formulario = this.fb.group({
      id: [country.id, Validators.compose([Validators.required])],
      name: [country.name, Validators.compose([Validators.required])]
    });
  }

  createForm() {
    this.formulario = this.fb.group({
      id: [null],
      name: [null, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(60)])]
    });
  }

  createCountry() {
    const THIS = this;

    this.countryService.createCountry(this.formulario.value).subscribe({
      next(success) {
        THIS.loading = false;
        THIS.toast.success('País cadastrado com sucesso', 'Sucesso!', {
          closeButton: true
        });
        THIS.submitForm = false;
        THIS.formulario.reset();
      },
      error(error) {
        THIS.loading = false;
        THIS.countryService.messagesError(error, THIS.countryService.resourceName);
      }
    });
  }

  editCountry() {
    const THIS = this;

    this.countryService.updateCountry(this.formulario.value).subscribe({
      next(success) {
        THIS.loading = false;
        THIS.toast.success('País atualizado com sucesso', 'Sucesso!', {
          closeButton: true
        });
      },
      error(error) {
        THIS.loading = false;
        THIS.countryService.messagesError(error, THIS.countryService.resourceName);
      }
    });
  }

  clearForm() {
    this.submitForm = false;
    this.formulario.reset();
  }

  get er_name() {return this.formulario.get('name')}

}
