import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {RegisterService} from '../../../core/register.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private route: ActivatedRoute,
              private registerService: RegisterService) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      const token = params.get('token');
      localStorage.rpToken = token;
      this.registerService.changeHasToken(true);
    });
  }
}
