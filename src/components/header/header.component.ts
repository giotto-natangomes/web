import {Component, OnInit, Input, AfterContentInit, AfterViewInit} from '@angular/core';
import * as moment from 'moment';
import {ApiService} from '../../core/api.service';

@Component({
  selector: 'rp-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit {
  dateTime;
  userLogado;
  @Input() menu = [];
  active = false;

  constructor (
    private apiService: ApiService) { }

  ngOnInit() {
    this.dateTime = moment().format('DD/MM/YYYY HH:mm:ss');
    setInterval(() => {
      this.dateTime = moment().format('DD/MM/YYYY HH:mm:ss');
    }, 1000);
    this.getLoggedUser();
  }

  getLoggedUser() {
    if (localStorage.user === undefined) {
      this.me();
    } else {
      this.userLogado = localStorage.user;
    }
  }

  me() {
    const _this = this;
    this.apiService.getMe().subscribe({
      next(success) {
        localStorage.user = success['data'].name;
        _this.userLogado = localStorage.user;
      },
      error(error) {
        console.log(error);
      }
    });
  }

  logOut() {
    this.apiService.logOut();
    localStorage.clear();
  }
}
