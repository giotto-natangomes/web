import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataTableComponent } from './data-table/data-table.component';
import { LeftMenuComponent } from './left-menu/left-menu.component';
import { RouterModule } from '@angular/router';
import { DataContentComponent } from './data-content/data-content.component';
import { BtnBackComponent } from './btn-back/btn-back.component';
import { LoadComponent } from './load/load.component';



@NgModule({
  declarations: [
    DataTableComponent,
    LeftMenuComponent,
    DataContentComponent,
    BtnBackComponent,
    LoadComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    DataTableComponent,
    LeftMenuComponent,
    DataContentComponent,
    BtnBackComponent,
    LoadComponent
  ]
})
export class ComponentsModule { }