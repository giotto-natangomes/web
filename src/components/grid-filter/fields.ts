
export class Fields {
  type: string;
  properties: Array<object>;
  events: Array<object>;
  required: boolean;
  name: string;
  dependency: string;

  constructor(type: string, properties: Array<object>, events: Array<object>, required: boolean, name: string, dependency: string) {
    this.type = type;
    this.properties = properties;
    this.events = events;
    this.required = required;
    this.name = name;
    this.dependency = dependency;
  }
}
