import {Component, Input, OnInit} from '@angular/core';
import {Fields} from './fields';

@Component({
  selector: 'rp-grid-filter',
  templateUrl: './grid-filter.component.html',
  styleUrls: []
})

export class GridFilterComponent implements OnInit {

  @Input() filters: Fields[];
  displayFilters = true;

  constructor() { }

  ngOnInit() {
    console.log(this.filters);
    if (this.filters.length > 0) {
      this.createFilters(this.filters);
    } else {
      this.displayFilters = false;
    }
  }

  createFilters(filters) {
    filters.forEach((filter, index) => {
      console.log(filter, index, 'el - index');
    });
  }

}
