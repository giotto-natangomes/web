import { Component, OnInit, Input, AfterViewInit } from '@angular/core';

@Component({
  selector: 'rp-load',
  templateUrl: './load.component.html',
  styleUrls: ['./load.component.scss']
})
export class LoadComponent implements OnInit, AfterViewInit {
  @Input() status = false;
  constructor() { }

  ngOnInit() {
    
  }

  ngAfterViewInit() {
    this.mudarMensagem();
  }


  mudarMensagem() {
    const paragrafo = document.getElementById('mensagem');
    let mensagem = 0;
    
    if (this.status) {
      
      let msg = setInterval( () => {
        if( this.status ) {
          if ( mensagem === 0) {
            paragrafo.textContent = "Parece que está demorando um pouco..."
            mensagem = 1;
          } else {
            paragrafo.textContent = "Ainda carregando..."
            mensagem = 0;
          }
        } else {
          clearInterval(msg);
        }
        
      }, 5000 )

    }
    
  }

}
