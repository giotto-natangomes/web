import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'rp-data-content',
  templateUrl: './data-content.component.html',
  styleUrls: ['./data-content.component.scss']
})
export class DataContentComponent implements OnInit {
  @Input() pageTitle;
  @Input() icon;
  
  constructor() { }

  ngOnInit() {
  }

}
