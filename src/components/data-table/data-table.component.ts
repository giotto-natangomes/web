import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'rp-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.scss']
})
export class DataTableComponent implements OnInit {
  @Input() columns = [{}];
  @Input() data = [{}];
  @Input() actions = [];
  @Input() dataService;
  @Input() editId = 'id';
  @Input() deleteId = 'id';
  @Output() deleteRegister = new EventEmitter();
  constructor() { }

  ngOnInit() {

  }

  delete(registerId){
    this.deleteRegister.emit(registerId);
  }

}
