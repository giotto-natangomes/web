import { Injectable, NgModule } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { EncryptionService } from './encryption.service';

@Injectable()

export class HttpsRequestInterceptor implements HttpInterceptor {

    encrypt = new EncryptionService();

    intercept(
        req: HttpRequest<any>,
        next: HttpHandler,
    ): Observable<HttpEvent<any>> {
        let head:any = {};
        if( localStorage.rpToken != undefined ) {
            head.headers = req.headers.set('Access-Token', this.encrypt.get( localStorage.rpToken ));
        }

        // const dupReq = req.clone({
        //     headers: req.headers.set('Access-Token', localStorage.rpToken != undefined ? this.encrypt.get( localStorage.rpToken ) : ''),
        // });

        const dupReq = req.clone(head);
        
        return next.handle(dupReq);
    }
}

@NgModule({
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: HttpsRequestInterceptor,
            multi: true,
        },
    ],
})


export class Interceptor { }