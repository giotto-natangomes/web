import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {CommonService} from './common.service';
import {Login} from './login';
import {environment} from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService extends CommonService<Login> {
  constructor(protected http: HttpClient) {
    super(http);
    this.serviceName = 'account';
    this.resourceName = 'user';
  }
  getMe() {
    return this.find('/me');
  }

  // getData() {
  //   const data = [];
  //   return data;
  // }

  logOut() {
    this.find('/lougout');
    window.location.href = 'http://' + window.location.hostname  + ':' + environment.loginPort;
  }
}
