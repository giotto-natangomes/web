import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import {environment} from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(
    private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
    if (this.isLogged()) {
      return true;
    } else {
      window.location.href = 'http://' + window.location.hostname  + ':' + environment.loginPort;
      return false;
    }
  }

  isLogged() {
    return !!localStorage.rpToken;
  }
}
