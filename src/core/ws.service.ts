import { Injectable, EventEmitter } from '@angular/core';
import { environment } from '../environments/environment';
import { EncryptionService } from './encryption.service';
import { observable, Observable } from 'rxjs';
// import { EventEmitter } from 'events';

@Injectable({
    providedIn: 'root'
})

export class WsService {
    private encrypt: EncryptionService;

    wsUri;
    websocket;
    msg: EventEmitter<Object>;

    constructor() {
        this.encrypt = new EncryptionService();
        this.msg =  new EventEmitter();
    }

    setWsUri() {
        if (!!localStorage.rpToken) {
            this.wsUri = environment.ws + encodeURIComponent(this.encrypt.get(localStorage.rpToken));
        } else {
            this.wsUri = environment.ws + encodeURIComponent(this.encrypt.get(localStorage.rpTempToken));
        }

        this.initWs();
    }

    initWs() {
        this.websocket = new WebSocket(this.wsUri);
        // this.websocket.onopen = evt => { this.onOpen(evt) };
        this.websocket.onclose = evt => { this.onClose(evt) };
        this.websocket.onmessage =  evt => { this.onMessage(evt) };
        this.websocket.onerror = evt => { this.onError(evt) };
    }

    wpOpen(topic) {
        this.websocket.onopen = evt => { this.onOpen(evt, topic) };
    }

    onOpen(evt, topic) {
        this.doSend(topic);
    }
    
    onClose(evt) {
    
    }
    
    onMessage(evt) {
        this.msg.emit(evt.data);
        
    }

    onError(evt) {
        console.error(evt.data);
    }

    doSend(topic) {
        let objMsg = {action: "subscribe", topic: {"name": topic}}
        this.websocket.send(JSON.stringify(objMsg));
    }

    getMsg() {
        return this.msg;
    }
}