import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../environments/environment';
// import { EncryptionService } from './encryption.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import {ToastrService} from 'ngx-toastr';
import {errorCode}  from './error-code'

@Injectable({
  providedIn: 'root'
})

export class CommonService<T> {
  private baseUrl = environment.host;
  private httpOptions: any;
  private router: Router;
  private token: string;
  private toast: ToastrService
  errorCode: errorCode = new errorCode();
  serviceName: string;
  resourceName: string;

  constructor(protected http: HttpClient) {
    this.httpOptions = {
      headers: new HttpHeaders(
        {
          'Content-Type': 'application/json'
        }
      )
    };
  }

  private getUrl(path: string): string {
    return this.baseUrl + '/' + this.serviceName + '/' + this.resourceName + path;
  }

  create(entity: T, path: string = '') {
    return this.http.post<T>(this.getUrl(path), entity, this.httpOptions);
  }

  read(path: string = '') {
    return this.http.get<T>(this.getUrl(path), this.httpOptions);
  }

  update(entity: Object, path: string = '') {
    return this.http.put(this.getUrl(path), entity, this.httpOptions);
  }

  delete(path: string = '') {
    return this.http.delete(this.getUrl(path), this.httpOptions);
  }

  find(path: string = '') {
    return this.http.get<T>(this.getUrl(path), this.httpOptions);
  }

  redirectLogin() {      
    Swal.fire({
      title: 'Erro!',
      text: "Faça login novamente!",
      type: 'error',
      confirmButtonText: 'Ok'
    });

    setTimeout(() => {      
      window.location.href = 'http://' + window.location.hostname  + ':' + environment.loginPort + '/#/logout';
    }, 2000);     
  };

  messagesAlert(msg) {
    Swal.fire({
      title: 'Erro!',
      text: msg,
      type: 'error',
      confirmButtonText: 'Ok'
    });
  }

  messagesError(error, resourceName) {
    if (error.status === 401) {
      this.redirectLogin();
    } else {
      const typeError = `error_${error.status}`;
      this.messagesAlert(this.errorCode[typeError][resourceName]);
    }
  }
}
