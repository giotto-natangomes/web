import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {Generic} from './generic';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {
  private hasToken: Generic = new Generic(false);

  constructor() {
    this.hasToken.value = false;
  }

  watchHasToken(): Observable<any> {
    return of (this.hasToken);
  }

  changeHasToken(value: boolean): void {
    this.hasToken.value = value;
  }
}
